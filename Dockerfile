# Stage de build
FROM node:14-alpine as builder
WORKDIR /app
COPY package.json .
RUN npm ci
COPY . .
RUN npm run build

# Stage final
FROM nginx:alpine
COPY --from=builder /app/build /usr/share/nginx/html